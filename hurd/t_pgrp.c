#define _GNU_SOURCE

#include <hurd.h>
#include <stdio.h>
#include <sys/mman.h>

int
main()
{
  process_t proc;
  pid_t pgrp;
  size_t numpids = 20;
  pid_t pids_[numpids], *pids = pids_;
  error_t err;
  int i;

  proc = getproc();
  pgrp = 1;

  err = proc_getpgrppids (proc, pgrp, &pids, &numpids);
  if (err)
   {
     printf ("error: proc_getpgrppids\n");
     return 1;
   }

  for (i = 0; i < numpids; i++)
    printf ("%d ", pids[i]);
  printf("\n");

  if (pids != pids_)
    munmap (pids, numpids);

  return 0;
}

