#!/usr/bin/perl

use strict;

my $routers_filename = "conf.d/router/01_gforge_forwards";
open ROUTERS, $routers_filename || die $!;
my @gf_block = <ROUTERS>;
close ROUTERS;

while (<>) {
  print;
  last if /^\s*begin\s*routers\s*$/;
};
my $in_gf_block = 0;
my $gf_block_done = 0;
my @line_buf = ();
while (<>) {
  last if /^\s*begin\s*$/;
  if (/^# BEGIN GFORGE BLOCK -- DO NOT EDIT #/) {
    $in_gf_block = 1;
    push @line_buf, @gf_block unless $gf_block_done;
    $gf_block_done = 1;
  };
  push @line_buf, $_ unless $in_gf_block;
  $in_gf_block = 0 if /^# END GFORGE BLOCK #/;
};
push @line_buf, $_;
print @gf_block unless $gf_block_done;
print @line_buf;
while (<>) { print; };

