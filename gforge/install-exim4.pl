#!/usr/bin/perl

use strict;

# Handle the three configuration setups
my $config_type;

my $cfg_exim4 = "/etc/exim4/exim4.conf";
my $cfg_exim4_main;
my $cfg_exim4_router;

if (-e $cfg_exim4) {
  $config_type = "manual";
  $cfg_exim4_main = $cfg_exim4;
  $cfg_exim4_router = $cfg_exim4;
} else {
  my $split_config = "false";

  open FILE, "/etc/exim4/update-exim4.conf.conf" or die "cannot open file $!";
  while (<FILE>) {
    chomp;
    if (/dc_use_split_config/) {
      s/dc_use_split_config=//;
      s/'//g;
      $split_config = $_;
    }
  }
  close FILE;

  if ($split_config eq "false") {
    $config_type = "auto-monolithic";
    $cfg_exim4_main= "/etc/exim4/exim.conf.template";
    $cfg_exim4_router= "$cfg_exim4_main";
  } else {
    $config_type = "auto-split";
    $cfg_exim4_main = "/etc/exim4/conf.d/main/01_exim4-config_listmacrosdefs";
    $cfg_exim4_router = "/etc/exim4/cond.f/router/01_gforge_forwards";
  }
}

my $cfg_gforge_main = "$cfg_exim4_main.gforge-new";
my $cfg_gforge_router = "$cfg_exim4_router.gforge-new";

print "config: $config_type\n";

my $action = $ARGV[0];

if ($action eq "configure") {
  print "configuring\n";
} elsif ($action eq "configure-file") {
  print "configure-file\n";
} elsif ($action eq "purge") {
  print "purge\n";
} elsif ($action eq "purge-file") {
  print "purge-file\n";
} else {
  print "Usage: $0 {configure|configure-files|purge|purge-files}\n";
  exit 1;
}

open FILE, "/etc/exim4/exim4.cf" or die "cannot open file $!";

while (<FILE>) {
  print "l: $_";
}

close FILE;

