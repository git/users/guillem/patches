# vim:syn=make

variable = first

target: target-a target-b

target-a:
	echo command-a-1
# empty command follows
	
	# shell comment
	echo command-a-2
# blank line follows

	echo command-a-3

variable = middle

target-b:
	echo command-b-1
ifeq ($(variable),last)
	echo command-b-cond
endif
	echo command-b-var $(variable)

variable = last
